Hyve frontend
=============

Absolute base requirements consist of:

- **Material design spec** (just looks most modern & most easy to design for)
- **Signup page**
  - Login modal // button to link to a separate page for logins.
  - There's also going to be a _privacy policy_ checkbox that needs to present that legal information,
    ideally without having to transition to a new page but I might implement this anyway.
- **User dashboard**
  - Edit _bio_
  - Change _personal details_
  - Edit _connections_ for their hyve page
  - Mandatory enforcement of having _at least one connection_
  - Let users change their _theme_ sort of like twitter? Add _background images_, _change theme color_, etc etc _personalisation is good_
- **Other stuff**
  - Ability to promote &/or _like other user's pages_ through likes // shares
  - Modal view for users to _see their QR code_
    - print it off
    - order stickers

Possibly use a sidebar to change settings? Idk. There wouldn't be enough options at least as of yet
