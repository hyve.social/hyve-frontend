import {Container, Typography} from '@mui/material';
import {Box} from '@mui/system';
import React from 'react';
import metalmusic from './assets/metalmusic.jpg';

const Sx404 = () => (
	<Container maxWidth="lg">
		<Box borderColor="blanche">
			<Typography variant="h1">
        Hi! What led you to this place?
			</Typography>
			<Typography variant="caption" gutterBottom>
        No matter where you go, everyone&apos;s connected.
			</Typography>
		</Box>
		<img src={metalmusic}/>
	</Container>
);

export default Sx404;
