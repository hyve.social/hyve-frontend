import * as React from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import SignUp from './Components/SignUp/SignUp';
import Dashboard from './Dashboard';
import Sw404 from './404';

const App = () =>
	(
		<BrowserRouter>
			<Routes>
				<Route index element={<SignUp/>}/>
				<Route path="/dashboard" element={<Dashboard/>}/>
				<Route path="*" element={<Sw404/>}/>
			</Routes>
		</BrowserRouter>
	);

export default App;
