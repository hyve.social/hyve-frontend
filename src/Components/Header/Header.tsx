import React from 'react';
import Box from '@mui/system/Box';
import Link from '@mui/material/Link';
import {RiAppleLine} from 'react-icons/ri';
import Typography from '@mui/material/Typography';
import './header.css';

const siteLinks = ['/dashboard', '/profile', '/membership'];

const Header = () =>
	(
		<Box className="header">
			<RiAppleLine/>
			<Link href="/" style={{color: 'black'}}><Typography variant="subtitle1" gutterBottom> Hyve.social/ </Typography> </Link>
			{siteLinks.map((link: string) => (
				// eslint-disable-next-line react/jsx-key
				<Link underline="none" href={link}> <Typography className="header__links" variant="subtitle1"> {link} </Typography></Link>
			))}
		</Box>
	);

export default Header;
