import React from 'react';
import {RiCloseLine} from 'react-icons/ri';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import styled from '@emotion/styled';

// eslint-disable-next-line no-unused-vars
const onLoginSubmit = (data: any) => {
	console.log(data);
	// eslint-disable-next-line no-undef
	fetch('/api/v1/login')
		.then(result => {
			console.log(result);
		});
};

const Login = ({setIsOpen}: any) => {
	const CloseButton = styled.button`
		color: turquoise;
		border-color: turquoise;
		border-radius: 2px;
	`;
	return (
		<>
			<Container maxWidth="sm" className="centered">
				<Box className="modal">
					<Typography variant="h5" className="modal__header">
						Hello! This is the login modal.
					</Typography>
					<CloseButton className="closeBtn" onClick={() => setIsOpen(false)}>
						<RiCloseLine style={{marginBottom: '-3px'}} />
					</CloseButton>
					<Container maxWidth="sm" className="modalContent">
						<Typography variant="caption">
							Are you sure you want to delete the item?
						</Typography>
					</Container>
					<ButtonGroup variant="contained" aria-label="outlined primary button group" className="modalActions">
						<Box className="actionsContainer">
							<Button className="deleteBtn" onClick={() => setIsOpen(false)}>
								Delete
							</Button>
							<Button className="cancelBtn" onClick={() => setIsOpen(false)}>
								Cancel
							</Button>
						</Box>
					</ButtonGroup>
				</Box>
			</Container>
		</>
	);
};

export default Login;
