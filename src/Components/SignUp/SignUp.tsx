import React, {useRef} from 'react';
import {useForm, Controller} from 'react-hook-form';
// May need this later.
// import {useNavigate} from 'react-router-dom';
// import FormControl from '@mui/material/FormControl';
import Input from '@mui/material/Input';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import InputLabel from '@mui/material/InputLabel';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import './submit.css';

const SignUp = () => {
	const {control, handleSubmit} = useForm({
		defaultValues: {
			username: '',
			email: '',
			password: '',
			privacyCheck: false,
		},
	});

	const failureNotification = withReactContent(Swal);
	// Function to run if the signup wasn't successful.
	const failure = (result: any) => {
		failureNotification.fire({
			title: 'Signup failure',
			text: `Fuck! It failed. The API said: ${result}`,
			icon: 'error',
			timer: 3000,
		});
	};

	const form = useRef(null);
	// Navigation between pages when we need
	// const navigate = useNavigate();

	const onSignUpSumbit = (data: any) => {
		console.log(data);
		// eslint-disable-next-line
		fetch('/api/v1/register', data)
			.then((result: any) => {
				if (result.status !== 200) {
					failure(result.statusText);
				}

				result.text();
				console.log(result);
			})
			.catch((err: any) => {
				failure(err.statusText);
				console.error(err);
			});
	};

	return (
		<>
			<Container className="outer-cont">
				<Typography variant="h1" gutterBottom>
          Hyve.social/
				</Typography>
				<div className="signup-form-header">
					<h3>Create an account for free</h3>
					<p>Free forever. No payment needed.</p>
				</div>

				<form ref={form} onSubmit={handleSubmit(onSignUpSumbit)} className="form-signup">
					{/* Username Field */}
					<br/>
					<Controller
						name="username"
						rules={{required: true}}
						control={control}
						render={({field}) =>
							<Input
								placeholder="Username"
								{...field}
								required
							/>
						}
					/>
					<br />
					{/* Email Field */}
					<Controller
						name="email"
						rules={{required: true, pattern: {
							value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
							message: 'You need a valid email address!',
						}}}
						control={control}
						render={({field}) =>
							<Input
								placeholder="Email"
								{...field}
								required
							/>}
					/>
					<br />
					{/* Password Field */}
					<Controller
						name="password"
						control={control}
						rules={{required: true}}
						render={({field}) =>
							<Input
								{...field}
								type="password"
								placeholder="Password"
								required
							/>
						}/>
					<br />
					{/* Privacy Checkbox */}
					<Controller
						name="privacyCheck"
						rules={{required: true}}
						control={control}
						render={({field}) =>
							<Box sx={{margin: 'auto', display: 'flex'}}>
								<Input
									{...field}
									sx={{paddingLeft: '4px', paddingRight: '4px'}}
									type="checkbox"
									value=""
									id="defaultCheck1"
									required />
								<InputLabel className="form-check-label" htmlFor="defaultCheck1">
                  By creating an account, you agree to our Terms of Service
                  and Privacy Policy
								</InputLabel>
							</Box>
						}/>
					<br />
					<br />
					<input className="submit" type="submit"/>
					{/* This is hacky as shit please fix this later. */}
					<br />
					<br />
				</form>
				<div className="form-signup">
					<Button className="btn btn-success btn-md btn-block">
            Already have an account?
					</Button>
				</div>
			</Container>
		</>
	);
};

export default SignUp;
