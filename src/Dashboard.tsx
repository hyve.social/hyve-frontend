import React from 'react';
import Header from './Components/Header/Header';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

const Dashboard = () =>
	(
		<>
			<Header/>
			<Container maxWidth="sm" sx={{padding: 2}}>
				<Typography variant="h4" component="h1" gutterBottom>
					Ilu !
				</Typography>
			</Container>
		</>
	);

export default Dashboard;
