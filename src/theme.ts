import {createTheme} from '@mui/material/styles';

export const hyve = '#ff9148';
export const turquoise = '#58b09c';
export const apricot = '#ffeedb';
export const fogra29 = '#353535';
export const rose = '#ff6978';

const theme = createTheme({
	breakpoints: {},
	typography: {
		h3: {
			fontSize: '1.2rem',
			'@media (min-width:600px)': {
				fontSize: '1.5rem',
			},
		},
		fontFamily: [
			'-apple-system',
			'BlinkMacSystemFont',
			'"Segoe UI"',
			'Roboto',
			'"Helvetica Neue"',
			'Arial',
			'sans-serif',
			'"Apple Color Emoji"',
			'"Segoe UI Emoji"',
			'"Segoe UI Symbol"',
		].join(','),
	},
	palette: {
		primary: {
			main: hyve,
			contrastText: apricot,
		},
		secondary: {
			main: turquoise,
		},
		error: {
			main: rose,
		},
	},
});

export default theme;
